package redGreenYellow;
import java.io.*;
public class Density {


	public static char howDense(Double d)
	{
		char q;
		if(d>=75.0)
			q = 'y';
		else 
			q= 'n';
		return q;	
	}
	
	public static void main(String[] args) throws IOException
	{
		InputStreamReader isr=new InputStreamReader(System.in);
		BufferedReader br= new BufferedReader(isr);
		System.out.println("Enter traffic density");
		double d1= Double.parseDouble(br.readLine());
		char s= Density.howDense(d1);
		if (s=='y')
			System.out.println("green");
		else
			System.out.println("red");
	}

}
