package redGreenYellow;

import static org.junit.Assert.*;

import org.junit.Test;

public class DensityTest {

	@Test
	public void test() {
		double d1 = 82.0293857342050;
		char a = Density.howDense(d1);
		assertEquals('y',a);
	}

}
